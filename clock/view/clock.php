<style>
	#clock {
		position: relative;
		width: 15vw;
		height: 15vw;
		margin: 0 auto;
		padding: 0;
		background: url('<?php echo content_url() . '/wp_widget/clock/view/'?>img/clock.svg') center no-repeat;
		background-size: contain;
		list-style: none;
	}

	.sec,
	.min,
	.hr {
		position: absolute;
		width: 4vw;
		height: 15vw;
		top: 0px;
		left: calc(50% - 2vw);
	}

	.sec {
		background-image: url('<?php echo content_url() . '/wp_widget/clock/view/'?>img/seconds.svg');
		background-repeat: no-repeat;
		background-position: center 0;
		background-size: contain;
		z-index: 3;
	}

	.min {
		background-image: url('<?php echo content_url() . '/wp_widget/clock/view/'?>img/minutes.svg');
		background-repeat: no-repeat;
		background-position: center 0;
		background-size: contain;
		z-index: 2;
	}

	.hr {
		background-image: url('<?php echo content_url() . '/wp_widget/clock/view/'?>img/hours.svg');
		background-repeat: no-repeat;
		background-position: center 0;
		background-size: contain;
		z-index: 1;
	}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

<ul id="clock" class="clock_<?php echo str_replace(' ', '_', $instance['title']);?>">
	<li class="sec"></li>
	<li class="min"></li>
	<li class="hr"></li>

</ul>

<script>
	$(document).ready(function() {

		var clock_hr = new Date().getHours(),
			clock_min = new Date().getMinutes(),
			clock_sec = new Date().getSeconds(),
			class_name = '.clock_<?php echo str_replace(' ', '_', $instance['title']);?>',
			alarm_hr   =  parseInt('<?php echo $instance['alarm_hr']?>'),
			alarm_min  = parseInt('<?php echo $instance['alarm_min']?>');

		setInterval( function() {
			var seconds = new Date().getSeconds();
			var sdegree = seconds * 6;
			var srotate = "rotate(" + sdegree + "deg)";

			$("" + class_name + " .sec").css({"-moz-transform" : srotate, "-webkit-transform" : srotate});

		}, 1000 );

		setInterval( function() {
			var mins    = new Date().getMinutes(),
				mdegree = mins * 6,
				mrotate = "rotate(" + mdegree + "deg)";

			$("" + class_name + " .min").css({"-moz-transform" : mrotate, "-webkit-transform" : mrotate});

		}, 1000 );

		setInterval( function() {
			var hours   = new Date().getHours(),
				mins    = new Date().getMinutes(),
				hdegree = hours * 30 + (mins / 2),
				hrotate = "rotate(" + hdegree + "deg)";

			$("" + class_name + " .hr").css({"-moz-transform" : hrotate, "-webkit-transform" : hrotate});

		}, 1000 );


		setInterval( function() {
			var cur_hours   = new Date().getHours(),
				cur_minutes = new Date().getMinutes(),
				cur_seconds = new Date().getSeconds();

			if ((alarm_hr === cur_hours) && (alarm_min === cur_minutes) && (cur_seconds < 1)){
				alert('alarm');
			}
		}, 1000 );


	});
</script>
