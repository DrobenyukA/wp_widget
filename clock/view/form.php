
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">

    <h4>Set alarm:</h4>
    <label for="<?php echo $this->get_field_id( 'alarm_hr' ); ?>">Hours: </label>
    <select class="widefat" style="width: 45px;" id="<?php echo $this->get_field_id( 'alarm_hr' ); ?>" name="<?php echo $this->get_field_name( 'alarm_hr' ); ?>" type="text" value="<?php echo esc_attr( $alarm_hr ); ?>">
        <?php for ($i = 0; $i < 24; $i++){
            $i_val = ($i < 10) ? '0' . $i : $i;
            echo '<option value="' . $i . '">' . $i_val . '</option>';
        }?>
    </select>
    <label for="<?php echo $this->get_field_id( 'alarm_min' ); ?>">Minutes: </label>
    <select class="widefat" style="width: 45px;" id="<?php echo $this->get_field_id( 'alarm_min' ); ?>" name="<?php echo $this->get_field_name( 'alarm_min' ); ?>" type="text" value="<?php echo esc_attr( $alarm_min ); ?>">
        <?php for ($i = 0; $i < 60; $i++){
            $i_val = ($i < 10) ? '0' . $i : $i;
            echo '<option value="' . $i . '">' . $i_val . '</option>';
        }?>
    </select>
</p>
