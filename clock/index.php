<?php

class Clock_Widget extends WP_Widget {

    /**
     * Initialization of widget.
     */
    function __construct() {
        parent::__construct(

            'clock_widget', //  ID
            __( 'Clock Widget', 'clock_domain' ), // Name
            array( 'description' => __( 'Clock with alarm', 'clock_domain' ), ) // Description

        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        }

        /*���������� ����������*/
        require  get_template_directory() . '/../../wp_widget/clock/view/clock.php';
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'text_domain' );
        $alarm_hr = ! empty( $instance['alarm_hr'] ) ? $instance['alarm_hr'] : '- -';
        $alarm_min = ! empty( $instance['alarm_min'] ) ? $instance['alarm_min'] : '- -';
        require  get_template_directory() . '/../../wp_widget/clock/view/form.php';
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['alarm_hr'] = ( ! empty( $new_instance['alarm_hr'] ) ) ? strip_tags( $new_instance['alarm_hr'] ) : '-';
        $instance['alarm_min'] = ( ! empty( $new_instance['alarm_min'] ) ) ? strip_tags( $new_instance['alarm_min'] ) : '-';

        return $instance;
    }

} 
// register widget
function register_clock_widget() {
    register_widget( 'Clock_Widget' );
}
add_action( 'widgets_init', 'register_clock_widget' );